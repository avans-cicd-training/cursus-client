import { Component, OnInit } from '@angular/core';
import { AdalService } from 'adal-angular4';

@Component({
  selector: 'my-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  authenticated = false;

  constructor(
    private service: AdalService,
  ) {
  }

  ngOnInit() {
    // Handle callback if this is a redirect from Azure
    this.service.handleWindowCallback();
  }

  logOut() {
    this.authenticated = false;
    this.service.logOut();
  }


  logIn() {
    this.authenticated = true;
    if (!this.service.userInfo.authenticated) {
      this.service.login();
    }
  }

  getUserName(): string {
    return 'Avans student';
  }
}
