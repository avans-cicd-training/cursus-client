export class Cursus {
  id: number;
  naam: string;
  attitude: string;
  functieniveau: string;
  slagingscriterium: string;
  status: string;
  maxdeelnemers: number;
  beschrijving: string;
  cursusdata: {
    id: number;
    datum: string;
  }[];
  cursusdocenten: {
    id: number;
    email: string;
    naam: string;
  }[];
  cursuscursisten: {
    id: number;
    email: string;
    naam: string;
  }[];
  aanpassen: boolean;
  aangemeld: boolean;

  constructor() {
    this.cursusdata = [];
    this.cursusdocenten = [];
    this.cursuscursisten = [];
  }
}
